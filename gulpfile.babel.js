import gulp     from 'gulp';
import plugins  from 'gulp-load-plugins';
import browser  from 'browser-sync';
import rimraf   from 'rimraf';
import panini   from 'panini';
import yargs    from 'yargs';
import lazypipe from 'lazypipe';
import inky     from 'inky';
import fs       from 'fs';
import siphon   from 'siphon-media-query';

const $ = plugins();

// Look for the --production flag
const PRODUCTION = !!(yargs.argv.production);

// Build the "build" folder by running all of the below tasks
gulp.task('build',
  gulp.series(clean, pages, sass, images, inline));

// Build emails, run the server, and watch for file changes
gulp.task('watch',
  gulp.series('build', server, watch));

// Build emails
gulp.task('default',
  gulp.series('build'));

// Delete the "emails" folder
// This happens every time a build starts
function clean(done) {
  rimraf('build', done);
}

// Compile layouts, pages, and partials into flat HTML files
// Then parse using Inky templates
function pages() {
  return gulp.src([
    'src/emails/pages/**/*.html',
    '!src/emails/pages/archive/**/*.html'
  ])
    .pipe(panini({
      root: 'src/emails/pages',
      layouts: 'src/emails/layouts',
      partials: 'src/emails/partials',
      helpers: 'src/emails/helpers'
    }))
    .pipe(inky())
    .pipe(gulp.dest('build'));
}

// Reset Panini's cache of layouts and partials
function resetPages(done) {
  panini.refresh();
  done();
}

// Compile Sass into CSS
function sass() {
  return gulp.src('src/emails/assets/scss/app.scss')
    .pipe($.sass({
      includePaths: ['node_modules/foundation-emails/scss']
    }).on('error', $.sass.logError))
    .pipe($.if(PRODUCTION, $.uncss(
      {
        html: ['build/**/*.html']
      })))
    .pipe(gulp.dest('build/css'));
}

// Copy and compress images
function images() {
  return gulp.src([
    'src/emails/assets/img/**/*',
    '!src/emails/assets/img/archive/**/*'
  ])
    .pipe($.imagemin())
    .pipe(gulp.dest('./build/assets/img'));
}

// Inline CSS and minify HTML
function inline() {
  return gulp.src('build/**/*.html')
    .pipe($.if(PRODUCTION, inliner('build/css/app.css')))
    .pipe(gulp.dest('build'));
}

// Start a server with LiveReload to preview the site in
function server(done) {
  browser.init({
    server: 'build'
  });
  done();
}

// Watch for file changes
function watch() {
  gulp.watch('src/emails/pages/**/*.html')
    .on('all', gulp.series(pages, inline, browser.reload));
  gulp.watch(['src/emails/layouts/**/*', 'src/emails/partials/**/*'])
    .on('all', gulp.series(resetPages, pages, inline, browser.reload));
  gulp.watch(['../scss/**/*.scss', 'src/emails/assets/scss/**/*.scss'])
    .on('all', gulp.series(resetPages, sass, pages, inline, browser.reload));
  gulp.watch('src/emails/assets/img/**/*')
    .on('all', gulp.series(images, browser.reload));
}

// Inlines CSS into HTML, adds media query CSS into the
// <style> tag of the email, and compresses the HTML
function inliner(css) {
  css = fs.readFileSync(css).toString();
  let mqCss = siphon(css);

  let pipe = lazypipe()
    .pipe($.inlineCss, {
      applyStyleTags: false,
      removeStyleTags: true,
      preserveMediaQueries: true,
      removeLinkTags: false
    })
    .pipe($.replace, '<!-- <style> -->', `<style>${mqCss}</style>`)
    .pipe($.replace,
      '<link rel="stylesheet" type="text/css" href="css/app.css">', '')
    .pipe($.htmlmin, {
      collapseWhitespace: true,
      minifyCSS: true
    });

  return pipe();
}
