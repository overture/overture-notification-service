# Overture Email Service

**Email microservice for the Overture app**

Manages interactions with SendGrid and Mailchimp. Takes POSTS requests
from from the Overture API and sends transactional and marketing emails.

## High Level Overview

``` sh
.
├── log/ # Logs from node-bunyan -- pipe logs here
├── src/ # Main app directory (no compilation process--yet)
│   ├── config/ # Makes .env details accessible
│   ├── controllers/ # Controllers define route actions & trigger emails
│   ├── middleware/ # CORS, body-parser, etc.
│   ├── routes/ # API request response routing. Each calls a controller
│   ├── utils/ # System wide error handling and tools
│   └── emails/ # Source email templates (Work here)
│       ├── assets/ # Image assets & SASS files
│       ├── layouts/ # Wrappers for indivual templates
│       ├── pages/ # Individual template bodies
│       └── partials/ # Recurring elements
└── emails # Compiled/Inlined email templates
    ├── assets/ # Minified images
    └── css/ # Minified css
```

## Email Service Installation Steps

This is the main service. You will need to get a .env to run/work in this part of the repo.

To run the app:

``` sh
yarn install
yarn start
```

## Email Builder Installation Steps

There's an installation of [Foundation for Emails (SASS)](https://foundation.zurb.com/emails/docs/sass-guide.html) at src/emails/. It's used for copywriting, laying out emails, and inlining.

To use the local Foundation development server:

``` sh
yarn watch
yarn build
```

The built emails are placed in the top level folder, `emails/`

For an example of how an email is sent, look at `src/controllers/sendGridTestController.js`
You will also need to create a route to trigger the email at `src/routes/routes.js`

## Postman (API Testing)

You can find the routes ready for testing with headers and request bodies already set here.

**[Postman API Route Collection](https://www.getpostman.com/collections/c92a9f79d48fdc38b23f)** (Importable)

**Postman Environment** (Importable as .json file)
``` json
{"id":"e159b4a7-ce9a-5155-f578-3fefc9774f63","name":"Overture Email Service","values":[{"key":"url","value":"localhost:4000/","description":"","type":"text","enabled":true}],"timestamp":1512798639337,"_postman_variable_scope":"environment","_postman_exported_at":"2017-12-11T09:45:14.942Z","_postman_exported_using":"Postman/5.3.2"}
```