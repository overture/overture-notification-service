import pkg from '../../package.json';
import dotenv from 'dotenv';

// init the env vars
dotenv.load();

const {
  SENDGRID_API_KEY,
  NODE_MAILER_EMAIL,
  NODE_MAILER_USER,
  NODE_MAILER_PASS,
  NODE_ENV,
  LOG_LEVEL
} = process.env;

export default {
  SENDGRID_API_KEY,
  NODE_MAILER_EMAIL,
  NODE_MAILER_USER,
  NODE_MAILER_PASS,
  env:                  NODE_ENV || 'development',
  port:                 4000,
  logger: {
    basePath:           './log',
    level:              LOG_LEVEL || 'info',
    name:               pkg.name || 'main-app',
    serviceLog:         'service.log',
  }
}
