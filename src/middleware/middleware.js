import bodyParser from "body-parser";
import cors from 'cors';
import { requestLogger } from 'utils/logUtils';
import { attachRequestId, useCors } from './requestMiddleware';

export default (app, log) => {
  app.use(useCors);
  app.use(attachRequestId);
  app.use(requestLogger);
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
}
