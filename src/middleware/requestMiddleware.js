import uuid from 'uuid/v1';

export function attachRequestId(req, res, next) {
  req.req_id = uuid();
  req.headers['Request-Id'] = req.req_id;
  res.setHeader('Request-Id', req.req_id);
  next();
}

export function useCors(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
}
