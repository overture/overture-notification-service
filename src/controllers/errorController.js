import log from 'utils/logUtils';

export function errorHandler(req, res) {
  res.status(404).json({
    errors: [{
      title: 'Not Found',
      detail: 'The resource you requested was not found.'
    }]
  });
}
