import Joi from 'joi';
import _ from "lodash";

import log from 'utils/logUtils';
import { SendClient } from 'utils/sendGrid';
import { NewSubscriberSchema } from 'validators/subscribeValidators';

export function createSubscriber(req, res) {
  const { recipients } = req.body;
  const { error } = NewSubscriberSchema.validate(recipients);

  if (error) {
    return res.status(400).json({
      errors: [error]
    });
  }

  saveMember(recipients)
    .then(([response, body]) => (
      res.status(response.statusCode).json(body)
    ))
    .catch(error => {
      log.error(error);
      res.status(500).json(body);
    });
}

function saveMember(recipients) {
  return SendClient.request({
    method: 'POST',
    url: '/v3/contactdb/recipients',
    body: recipients
  });
}
