import handlebars from 'handlebars';
import fs from 'fs';
import _ from 'lodash';

import config from 'config/config';
import logger from 'utils/logUtils';
import { SendMail } from 'utils/sendGrid';
import emailContent from 'data/emailContentMap';
import emailValidators from 'validators/sendMailValidators';

const log = logger.child({ feature: 'send-mail' });

export function sendMail(req, res) {
  const content = emailContent[req.body.email_type];
  const emailSchema = emailValidators[content.validator];
  const { error } = emailSchema.validate(req.body);

  if (error) {
    return res.status(400).json({
      errors: [error]
    });
  }

  fs.readFile(`build/${content.template}`, { encoding: 'utf-8' }, (err, html) => {
    if (err) {
      return log.error(err);
    }

    const template = handlebars.compile(html);
    const environment = {
      DEBUG_ENABLED: true,
      env: config.env
    };

    SendMail
      .send({
        to: `${req.body.name}<${req.body.email}>`,
        from: content.from,
        subject: content.subject,
        text: content.text,
        html: template({ payload: req.body, ...environment })
      })
      .then(resp => res.status(201).json())
      .catch(error => {
        log.error(error);
        res.status(500).json({
          errors: [error]
        });
      });
  });
}
