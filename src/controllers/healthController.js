import os from 'os';
import moment from 'moment';
import pkg from '../../package.json';
import config from '../config/config';

export function doHealthCheck(req, res) {
  const loadAverage = os.loadavg();
  const { rss, heapTotal, heapUsed } = process.memoryUsage();

  res.status(200).json({
    time           : moment().utc().format('YYYY-MM-DD HH:mm:ss.SS'),
    serviceName    : pkg.name,
    serviceVersion : pkg.version,
    nodeVersion    : process.version,
    platform       : os.platform(),
    arch           : os.arch(),
    release        : os.release(),
    loadAvg01Min   : loadAverage[0].toFixed(3),
    loadAvg05Min   : loadAverage[1].toFixed(3),
    loadAvg15Min   : loadAverage[2].toFixed(3),
    totalMem       : os.totalmem(),
    freeMem        : os.freemem(),
    rss            : rss,
    heapTotal      : heapTotal,
    heapUsed       : heapUsed,
    uptime         : process.uptime(),
    hostname       : os.hostname(),
    pid            : process.pid,
    port           : config.port
  });
}
