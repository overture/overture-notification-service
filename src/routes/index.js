import express from 'express';

import { createSubscriber } from '../controllers/subscribersController';
import { sendMail } from '../controllers/sendMailController'
import { doHealthCheck } from '../controllers/healthController';
import { errorHandler } from '../controllers/errorController';

const router = express.Router();

router.post('/sendmail', sendMail);
router.post('/subscribe', createSubscriber);
router.get('/heartbeat', doHealthCheck);
router.get('*', errorHandler);

export default router;
