import config from  'config/config';
import express from 'express';
import log from 'utils/logUtils';
import errorUtils from 'utils/errorHandlerUtils';
import middleware from 'middleware/middleware';
import routes from 'routes';

const app = express();

middleware(app);

app.use('/api/v1', routes);
app.listen(config.port, err => log.info(`Listening on http://localhost/${config.port}`));

export default app;
