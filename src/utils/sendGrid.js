// https://github.com/sendgrid/sendgrid-nodejs
import SendGridMail from '@sendgrid/mail';
import SendGridClient from '@sendgrid/client';
import config from 'config/config';

SendGridMail.setApiKey(config.SENDGRID_API_KEY);
SendGridClient.setApiKey(config.SENDGRID_API_KEY);

export const SendMail = SendGridMail;
export const SendClient = SendGridClient;
