import nodemailer from 'nodemailer';
import config from 'config/config';
import log from './logUtils';
import { generateUnhandledExceptionEmail } from './emails/unhandledException'

const transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: config.NODE_MAILER_USER,
    pass: config.NODE_MAILER_PASS
  }
});

process.on('uncaughtException', excep => {
  log.fatal(excep);

  if (config.env === 'production') {
    transporter.sendMail({
      from: `Overture Micro Services <${ config.NODE_MAILER_USER }>`,
      to: `<${ config.NODE_MAILER_EMAIL }>`,
      subject: 'Fatal - OES: Overture Email Service',
      text: 'Fatal error in microservice OES',
      html: generateUnhandledExceptionEmail(excep)
    }, (error, info) => {
      if (error) {
        log.error('Fatal nodemailer error', error);
        return;
      }

      log.error('Fatal nodemailer error', error);
    });
  }

  // kill the process
  setTimeout(function() {
    process.exit(1);
  }, 1000);
});
