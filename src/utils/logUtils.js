import path from 'path';
import bunyan from 'bunyan';
import moment from 'moment';
import _ from 'lodash';
import conf from 'config/config';

let mainLoggerStreams = [{
  stream: process.stdout
}];

if (conf.env === 'production') {
  mainLoggerStreams = [{
    path: path.resolve(conf.logger.basePath, 'service.log')
  }];
}

// main logger
const logger = bunyan.createLogger({
  name: conf.logger.name,
  level: conf.logger.level,
  serializers: bunyan.stdSerializers,
  streams: mainLoggerStreams
});

// request logger
export function requestLogger(req, res, next) {
  const oldEnd = res.end;
  const reqChildConfig = {
    req_id: req.req_id.split('-')[0]
  };

  req.log = logger.child({ ...reqChildConfig, tag: 'req' });
  res.log = logger.child({ ...reqChildConfig, tag: 'res' });

  res.log.info({ req: req }, '< %s: %s',
    req.method,
    req.originalUrl
  );

  res.end = function() {
    let level = 'info';

    if (res.statusCode >= 400) {
      level = 'warn';
    }

    if (res.statusCode >= 500) {
      level = 'error';
    }

    res.log[level]({ res: res }, '> %s: %s %d',
      req.method,
      req.originalUrl,
      res.statusCode,
    );

    // extends the old end method
    oldEnd.apply(res, arguments);
  }

  next(null);
}

export default logger;
