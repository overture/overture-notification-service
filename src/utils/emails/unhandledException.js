import moment from 'moment';
import pkg from '../../../package.json';

export function generateUnhandledExceptionEmail(excep) {
  const stack = excep.stack.split('\n');
  return (`
    <p>
      This message was generated at ${ moment().format('MMMM Do YYYY, h:mm:ss a') } by
      <span style="font-weight: bold;">${ pkg.name }</span>
    </p>
    <div style="border: 1px solid #3e2856;">
      <h3 style="padding: 10px; margin: 0; background-color: #3e2856; color: white; font-weight: 400;">${stack[0]}</h3>
      <ul style="margin-top: 0; list-style: none; padding: 15px;">
        ${
          stack
            .map(line => `<li>${line.trim()}</li>`)
            .join('')
        }
      </ul>
    </div>
  `);
}
