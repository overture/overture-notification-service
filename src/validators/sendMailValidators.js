import Joi from 'joi';

const EmailBaseSchema = Joi.object({
  email_type: Joi.string().required(),
  email: Joi.string().required()
});

export const PasswordResetSchema = EmailBaseSchema.keys({
  token: Joi.string().uuid().required(),
  first_name: Joi.string().max(25, 'utf8')
});

export default {
  PasswordResetSchema
};
