import Joi from 'joi';

const recipientSchmeaMap = {
  age: Joi.number(),
  email: Joi.string().required(),
  first_name: Joi.string().max(25, 'utf8'),
  last_name: Joi.string().max(25, 'utf8'),
  is_beta: Joi.number().integer().min(0).max(1)
};

export const NewSubscriberSchema = Joi.array().items(
  Joi.object(recipientSchmeaMap)
).min(1).label('Recipients');
